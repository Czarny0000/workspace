package pl.sda.figury.Bibliotekazadanie;

public class Autor extends Człowiek   {
    private String jezyk;

    public Autor(String imie, String nazwisko, String jezyk) {
        super(imie, nazwisko);
        this.jezyk = jezyk;
    }

    @Override
    public String toString() {
        return "Auto" + super.toString();
    }
}
