package pl.sda.figury.Bibliotekazadanie;

public class Biblioteka {
    public Egzemplarz[] zbior;

    public Biblioteka(Egzemplarz[] zbior) {
        this.zbior = zbior;
    }

    public Egzemplarz[] szukajPoTytule(String tytul) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;

        for (Egzemplarz egzemplarz : zbior) {
            if (egzemplarz.pobierzTytul().contains(tytul)) {
                znalezione[iloscZnalezionych] = egzemplarz;
                iloscZnalezionych++;

            }
        }
        return przytnij(znalezione);

    }


    private Egzemplarz[] przytnij(Egzemplarz[] pozycje) {
        int niePustePozycje = 0;
        for (Egzemplarz egzemplarz : pozycje) {
            if (egzemplarz != null) {
                niePustePozycje++;
            }
        }

        Egzemplarz[] przycieta = new Egzemplarz[niePustePozycje];
        for (int i = 0; i < przycieta.length; i++) {
            przycieta[i] = pozycje[i];
        }
        return przycieta;
    }

    public Egzemplarz[] szukajPoAutorze(String autor) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;
        for (Egzemplarz egzemplarz : zbior) {
            for (Autor aut : egzemplarz.autorzy) {
                if (aut.toString().contains(autor)) {
                    znalezione[iloscZnalezionych] = egzemplarz;
                    iloscZnalezionych++;
                }

            }
        }
        return przytnij(znalezione);


    }

    public Egzemplarz[] szukajPoTytulelubAutor(String tytul) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;

        for (Egzemplarz egzemplarz : zbior) {
            if (egzemplarz.toString().contains(tytul)) {
                znalezione[iloscZnalezionych] = egzemplarz;
                iloscZnalezionych++;
            }
        }
        return przytnij(znalezione);
    }
}