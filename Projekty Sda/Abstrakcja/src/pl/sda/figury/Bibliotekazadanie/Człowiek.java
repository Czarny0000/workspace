package pl.sda.figury.Bibliotekazadanie;

public class Człowiek {
    protected  String imie, nazwisko;

    public Człowiek(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return String.format("%s %s", imie, nazwisko);
    }
}
