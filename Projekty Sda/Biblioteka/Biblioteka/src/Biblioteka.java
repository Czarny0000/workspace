import java.util.ArrayList;

public class Biblioteka {


    ArrayList<Ksiazka> listaKsiazek = new ArrayList<>();
    ArrayList<Ksiazka> listaWypoKsiazek = new ArrayList<>();

    public Biblioteka(ArrayList<Ksiazka> listaKsiazek) {
        this.listaKsiazek = listaKsiazek;
    }

    public  boolean czyKsiazkaDostepna(String nazwa){
        boolean czyKsiazkaDostepna=false;
        for (Ksiazka ksiazka:listaKsiazek) {
            if (nazwa==ksiazka.getNazwa())
                czyKsiazkaDostepna=true;
        }
        return czyKsiazkaDostepna;

    }

    public Ksiazka wypozyczKsiazke(String nazwa) {
        Ksiazka wypo = null;
        if (czyKsiazkaDostepna(nazwa)) {
            int pozycjaKsiazki = 0;

            for (int i = 0; i < listaKsiazek.size(); i++) {

                if (nazwa == listaKsiazek.get(i).getNazwa()) {
                    pozycjaKsiazki = i;
                    wypo = listaKsiazek.get(i);
                }


            }
            listaKsiazek.remove(pozycjaKsiazki);

            listaWypoKsiazek.add(wypo);
        }
        return wypo;
    }

    public void wyswpitlDostepne(){
        for (Ksiazka ksiazka:listaKsiazek) {
            System.out.println(ksiazka);
        }
    }

    public void wyswpitlWypozyczone(){
        for (Ksiazka ksiazka:listaWypoKsiazek) {
            System.out.println(ksiazka);
        }
    }
}
