package pl.sda.auta;

import java.util.Random;

public class Samochod {
    int predkosc = 0;
    boolean czyWlaczoneSwiatla;
    int aktualnaPredkosc;
    boolean czySchowanyDach;
    String nazwa;
    protected String kolor;
    protected int rocznik;

    public Samochod(String nazwa, String kolor, int rocznik) {
        this.nazwa = nazwa;
        this.kolor = kolor;
        this.rocznik = rocznik;
    }

    public void przyspieszODziesiec() {

       // while(aktualnaPredkosc<120){

        if (predkosc < 120) {
            this.aktualnaPredkosc += predkosc + 10;
            aktualnaPredkosc += predkosc;
            System.out.printf("Przyśpieszam do %d km/h\n", aktualnaPredkosc);
        }
        if (aktualnaPredkosc >= 120) {
            System.out.println("Ograniczenie do 120");
        }
          }

   // }

    public void wlaczSwiatla() {
        czyWlaczoneSwiatla = true;
    }


    public void setCzyWlaczoneSwiatla(boolean czyWlaczoneSwiatla) {
        this.czyWlaczoneSwiatla = czyWlaczoneSwiatla;
    }

    public void infoOSamochodzie() {
        //System.out.printf("Aktuala predkosc %d\n wlaczone swiatla " +czyWlaczoneSwiatla+
                //"\nCzy schowany dach"+czySchowanyDach,aktualnaPredkosc);
        //System.out.println(String.format(nazwa,kolor,rocznik));
        System.out.printf("%s %d \nKolor %s \nAktuala predkosc %d \n",nazwa,rocznik,kolor, aktualnaPredkosc);

        if (czyWlaczoneSwiatla)
            System.out.println("Swiatla wlaczone");
        else
            System.out.println("Swiatla wyaczone");

        if (czySchowanyDach)
            System.out.println("Dach schowany");
        else
            System.out.println("Dach zamkniety");
    }

    public void przyspieszDo120LOSOWE() {
        //while(aktualnaPredkosc<120){
        Random random = new Random();

        if (predkosc < 120) {
            this.aktualnaPredkosc += predkosc + random.nextInt(10);
            aktualnaPredkosc += predkosc;
            System.out.printf("Przyśpieszam do %d km/h %s\n", aktualnaPredkosc, nazwa);
        }
        if (aktualnaPredkosc >= 120) {
            System.out.printf("%s Osiągnales 120km/h\n", nazwa);
        }
//        }
    }

    @Override
    public String toString() {
        return String.format("%s samochod marki %s rocznik %d", kolor, nazwa, rocznik);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod){
            Samochod ten = (Samochod) obj;
            if (this.nazwa.equals(ten.nazwa) && this.kolor.equals(ten.kolor)){
                return true;
            }
        }
        return false;
    }


}
