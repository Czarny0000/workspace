package pl.sda.oklo;

public class Kolo {
    double promien;
    protected final double LICZBA_PI=3.14;

    public Kolo(double promien) {
        this.promien = promien;
    }
    public final double obliczpole(){
        double pole =LICZBA_PI*(promien*promien);
        return pole;
    }
}
