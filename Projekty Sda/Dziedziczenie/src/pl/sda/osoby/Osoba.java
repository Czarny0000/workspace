package pl.sda.osoby;

public class Osoba {
    String imie;
    String nazwisko;
    int wiek;

    public Osoba(String imie, String nazwisko, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
    }

    public void przedstawSie(){
        System.out.printf("Czesc nazywan sie %s %s i mam %d lat\n",imie,nazwisko,wiek);
    }
}
