import java.util.Random;

public class Impreza {

    private GatunekPiwa[] tabPiw= new GatunekPiwa[2];

    public Impreza() {


        Random r = new Random();
        int a=r.nextInt(5);
        int b=r.nextInt(5);

        switch(a) {
            case 0:
                tabPiw[0] = GatunekPiwa.LAGER;
                break;
            case 1:
                tabPiw[0] = GatunekPiwa.PILZNER;
                break;
            case 2:
                tabPiw[0] = GatunekPiwa.STOUT;
                break;
            case 3:
                tabPiw[0] = GatunekPiwa.PORTER;
                break;
            case 4:
                tabPiw[0] = GatunekPiwa.MIODOWE;
                break;
        }

        switch(b) {
            case 0:
                tabPiw[1] = GatunekPiwa.LAGER;
                break;
            case 1:
                tabPiw[1] = GatunekPiwa.PILZNER;
                break;
            case 2:
                tabPiw[1] = GatunekPiwa.STOUT;
                break;
            case 3:
                tabPiw[1] = GatunekPiwa.PORTER;
                break;
            case 4:
                tabPiw[1] = GatunekPiwa.MIODOWE;
                break;
        }
    }
    public String dostepnePiwa(){
        String dostepnePiwa= "Dostępne piwa to:"+tabPiw[0].opisPiwa()+", "+tabPiw[1].opisPiwa();
        return dostepnePiwa;
    }
}
