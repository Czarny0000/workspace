public class Pracownik {

    String imie;
    String Nazwisko;
    double miesieczneWynagrodzenie;


    public Pracownik(String imie, String nazwisko, double miesieczneWynagrodzenie) {
        this.imie = imie;
        Nazwisko = nazwisko;
        this.miesieczneWynagrodzenie = miesieczneWynagrodzenie;
    }

    @Override
    public String toString() {
        return "Pracownik " +
                "" + imie +" "+
                  Nazwisko +
                " miesieczne Wynagrodzenie = " + miesieczneWynagrodzenie
                ;
    }
}
