public enum Figura {
    AS("As"),
    KROL("Król"),
    DAMA("Dama"),
    WALET("Walet"),
    DZIESIATKA("Dziesiatka"),
    DZIEWIATKA("Dziewiatka"),
    OSEMKA("Osemka"),
    SIODEMKA("Siodemka");

    String nazwa;

    Figura(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNazwa(){
        return nazwa;
    }
}
