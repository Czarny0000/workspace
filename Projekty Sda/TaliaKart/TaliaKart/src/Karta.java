public class Karta {
    Kolor kolor;
    Figura figura;

    public Karta(Kolor kolor, Figura figura) {
        this.kolor = kolor;
        this.figura = figura;
    }

    @Override
    public String toString() {
        String karta =figura.getNazwa()+" "+kolor.getNazwa();
        return  karta;

    }
}
