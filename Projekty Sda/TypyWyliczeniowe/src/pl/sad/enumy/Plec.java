package pl.sad.enumy;

public enum Plec {
    KOBIETA("♀"),
    MEZCZYZNA("♂");

    String symbol;


    Plec(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
