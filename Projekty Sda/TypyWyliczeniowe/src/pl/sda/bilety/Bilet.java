package pl.sda.bilety;

public enum Bilet {

    ULGOWY_GODZINNY(1.6, 60),
    ULGOWY_CALODNIOWY(5.2, 24 * 60),
    NORMALNY_GODZINNY(3.2, 60),
    NORMALNY_CALODNIOWY(10.40, 24 * 60),
    BRAK_BILETU(0.0, 0);

    private double cena;
    private int czasWMinutach;

    Bilet(double cena, int czasWminutach) {
        this.cena = cena;
        this.czasWMinutach = czasWminutach;
    }

    public double pobierzCeneBiletu() {
        return cena;
    }

    public int pobierzCzasJazdy() {
        return czasWMinutach;
    }

    public void wyswietlDaneOBilecie() {
        // System.out.printf("Bilet %s %d-godzinny",this.toString().split("_")[0].toLowerCase(), czasWMinutach/60);

        String nazwa = this.toString();
        nazwa = nazwa.toLowerCase();
        nazwa = nazwa.split("_")[0];
        System.out.println(String.format("Bilet %s cena %s %d- godzinny", nazwa,cena,czasWMinutach / 60));
    }

    public static Bilet kupBilet(int wiek, int czasWMinutach, double kwota) {
        Bilet bilet = Bilet.BRAK_BILETU;

        if (wiek <= 18 || wiek >= 65) {
            if (czasWMinutach >= 60) {
                bilet = Bilet.ULGOWY_CALODNIOWY;
            } else {
                bilet = Bilet.ULGOWY_GODZINNY;
            }
        } else {
            if (czasWMinutach >= 60) {
                bilet = Bilet.NORMALNY_CALODNIOWY;
            } else {
                bilet = Bilet.NORMALNY_GODZINNY;
            }
        }
            if (bilet.pobierzCeneBiletu() <= kwota) {

                return bilet;
            }
        return Bilet.BRAK_BILETU;
    }
}

