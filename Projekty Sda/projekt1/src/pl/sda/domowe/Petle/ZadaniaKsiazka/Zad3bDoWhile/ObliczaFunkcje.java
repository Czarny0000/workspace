package pl.sda.domowe.Petle.ZadaniaKsiazka.Zad3bDoWhile;

/**
 * program, który za pomocą instrukcji do ... while dla
 * danych wartości x zmieniających się od 0 do 10 oblicza wartość
 * funkcji y = 3x.
 */

public class ObliczaFunkcje {
    public static void main(String[] args)
    {
        int x = 0, y = 0; // ustalenie wartości początkowych
        System.out.println("Program oblicza wartość funkcji y = 3*x");
        System.out.println("dla x zmieniającego się od 0 do 10.");
        do
        {
            y = 3*x;
            System.out.println(" x = " + x + "\t" + " y = " + y);
            x++;
        }
        while (x <= 10);
    }
}
