package pl.sda.domowe.Petle.ZbiorPierwszy.zad2;

import java.util.Scanner;

/**
 * drukuje napis dopuki nie podamy liczby ujemnej
 */

public class petleDoWhile1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int liczba;


        do {
            System.out.println("podaj liczbe");
            liczba = scanner.nextInt();

                if (liczba>0)
                    System.out.println("\nHelo World\n");
                else
                    System.out.println("koniec");
        }while(liczba>0);

    }

}
