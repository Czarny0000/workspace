package pl.sda.domowe.Stringi.KlasaStringWyrażeniaRegularne;

import java.util.Scanner;

public class Stringi2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz zdanie");
        String tekst=scanner.nextLine();
        System.out.println("Podaj słowo");
        String searched = scanner.next();

        if (tekst.contains(searched))
            System.out.println("znaleziono szukane słowo "+"'"+searched+"'");
        else if (!tekst.contains(searched))
            System.out.println("Nie znaleziono słowa "+searched);
        System.out.println(tekst.contains("Ania"));
        System.out.println((tekst.startsWith("Ania")));
        System.out.println(tekst.endsWith("Ania"));
        System.out.println(tekst.equals("Ania"));
        String tekst1 = tekst.toLowerCase();
        System.out.println(tekst1.contains("Ania"));
        System.out.println(tekst.indexOf("Ania"));
    }
}
