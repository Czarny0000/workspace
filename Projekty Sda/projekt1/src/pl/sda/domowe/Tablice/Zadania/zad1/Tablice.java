package pl.sda.domowe.Tablice.Zadania.zad1;

import java.util.Arrays;
import java.util.Random;

public class Tablice {
    public static void main(String[] args) {
        Random gen = new Random();
        int x = -10;
        int y = 10;

        double tab[] = new double[10];
        for (int i = 0; i < tab.length; i++) {
            int a;
            a = gen.nextInt(y - x + 1) + x;
            tab[i] = a;
            System.out.println(tab[i]);
        }
        double max = tab[0];
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > max)
                max = tab[i];
        }
        System.out.println("Watrosc maksymalna = " + max);
        double min = tab[0];
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < min)
                min = tab[i];
        }
        System.out.println("Wartosc minimalna = " + min);

        double suma = 0;
        for (double iter : tab) {
            suma += iter;
        }
        System.out.println("Suma wszystkich wartości: " + suma);

        // Obliczanie średniej
        double srednia = suma / tab.length;
        System.out.println("Średnia pomiarów: " + srednia);

        System.out.println("Wartości większe od średniej");

        for (double iter : tab) {
            if (iter > srednia)
                System.out.print(iter + ", ");
        }

        System.out.println("\nWartości mniejsze od średniej");

        for (double iter : tab) {
            if (iter < srednia)
                System.out.print(iter + ", ");

        }

        System.out.println("\nWartosci w odwrotnej kolejności");
        for (int i = tab.length - 1; i >= 0; i--) {
            System.out.println(tab[i]);
        }

        Arrays.sort(tab);
        System.out.println("Posortowane wyniki");
        System.out.println(Arrays.toString(tab));
        System.out.println("Mediana");
        System.out.println((tab[4]+tab[5])/2);

        double mediana;

        if (tab.length % 2 == 0) {

            mediana=(((tab[(tab.length/2)-1])+(tab[tab.length/2]))/2);
            System.out.println(mediana);
            }
            else
                mediana=tab[(tab.length-1)/2];



    }

}

