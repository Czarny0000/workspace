package pl.sda.domowe.Tablice.Zadania.zad2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Tab wielkosci 20, wypelniona random z przedzialu od 1 do 10, nastepnie posortowana
 * i obliczona najczesciej wystepujaca liczba.
 */

public class Tablica2 {
    public static void main(String[] args) {
        Random gen = new Random();
        Scanner csanner = new Scanner(System.in);
        int c ;
        int x = 1;
        int y = 10;
        int tab[] = new int[20];

        for (int i = 0; i < tab.length; i++) {
            int a;
            a = gen.nextInt(y - x + 1) + x;
            tab[i] = a;
            System.out.println(tab[i]);

        }
        System.out.println(Arrays.toString(tab));

        Arrays.sort(tab);
        System.out.println(Arrays.toString(tab));

        int powtOstatniej = 0;
        int powtorzenia = 0;
        int najczęstszaWartość = tab[0];                    //Działa w posortowanej tablicy!!
                                                            //Jeśli zartość pod tab[i] jest taka sama jak pod tab[i-1]
                                                            //to liczba powt zwieksza się
                                                 //Jesli nie wystapi powtorzenie...
                 //KURWA NIE WIEM JAK TO OPISAC ALE DZIAŁA... DO POZNIEJSZEGO KOPIOWANIA W RAZIE POTRZEBY
        for (int i = 1; i < tab.length; i++) {
            if (tab[i] == tab[i - 1]) {
                powtorzenia++;
            } else {
                if (powtorzenia > powtOstatniej) {
                    najczęstszaWartość = tab[i - 1];
                    powtOstatniej = powtorzenia;
                }
                powtorzenia = 0;
            }
        }
        System.out.println(najczęstszaWartość);

    }
}

