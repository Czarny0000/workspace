package pl.sda.domowe.Tablice.ZadaniaKsiazka.Zad4;

/**
 * program, który w zadeklarowanej tablicy dwuwymiarowej
 * 10×10 o nazwie macierz umieszcza na przekątnej liczbę 1,
 * a poza przekątną 0. Dodatkowo program powinien obliczać
 * sumę elementów wyróżnionych w tablicy, tj. tych znajdujących
 * się na jej przekątnej.
 */

public class TabUmieszczaPoPrzekatnej {
    public static void main(String[] args)
    {
        int n = 10, i, j, suma;
        String  macierz[][] = new String[n][n];
// wpisywanie do tablicy 1 na przekątnej, a 0 poza przekątną
        for (i = 0; i < n; i++)
        {
            for(j = 0; j < n; j++)
            {
                if (i == j)
                    macierz[i][j] = "1";
                else if (i+1==j){
                    macierz[i][j] = "1";
                }
                else if (i+2==j){
                    macierz[i][j] = "1";
                }else if (i+3==j){
                    macierz[i][j] = "1";
                }else if (i+4==j){
                    macierz[i][j] = "1";
                }else if (i+5==j){
                    macierz[i][j] = "1";
                }else if (i+6==j){
                    macierz[i][j] = "1";
                }else if (i+7==j){
                    macierz[i][j] = "1";
                }else if (i+8==j){
                    macierz[i][j] = "1";
                }else if (i+9==j){
                    macierz[i][j] = "1";
                }
                else
                    macierz[i][j] = " ";
            }
        }
// wyświetlenie zawartości tablicy
        for (i = 0; i < n; i++)
        {
            for(j = 0; j < n; j++)
            {
                System.out.print(macierz[i][j] + " ");
            }
            System.out.println();
        }
//        suma = 0;
//        for (i = 0; i < n; i++)
//        {
//            suma = suma+macierz[i][i];
//        }
//        System.out.println("Suma wyróżnionych elementów w tablicy wynosi" + suma + ".");
    }
}
