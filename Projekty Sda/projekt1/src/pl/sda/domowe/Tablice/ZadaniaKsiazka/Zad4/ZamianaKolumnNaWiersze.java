package pl.sda.domowe.Tablice.ZadaniaKsiazka.Zad4;

/**
 * Tablica b zawiera same zera. Napisz program, który przepisuje zawartość
 * tablicy a do tablicy b (interpretacja graficzna tablicy wynikowej
 * poniżej), zamieniając kolumny na wiersze.
 */

public class ZamianaKolumnNaWiersze {
    public static void main(String[] args)
    {
        int n = 10, i, j;
        int a[][] = new int[n][n];
        int b[][] = new int[n][n];
// wpisywanie liczb do tablicy a
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < n; j++)
            {
                a[i][j] = j;
            }
        }
// przepisywanie liczb z tablicy a do tablicy b
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < n; j++)
            {
                b[i][j] = a[j][i]; // zamiana kolumn na wiersze
            }
        }
// wyświetlenie zawartości tablicy a
        System.out.println("Wyświetlenie zawartości tablicy a:");
        System.out.println();
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < n; j++)
            {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
// wyświetlenie zawartości tablicy b
        System.out.println();
        System.out.println("Wyświetlenie zawartości tablicy b:");
        System.out.println();
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < n; j++)
            {
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
    }
}
