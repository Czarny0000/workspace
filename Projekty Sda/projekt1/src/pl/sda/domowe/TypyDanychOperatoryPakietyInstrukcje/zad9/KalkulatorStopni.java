package pl.sda.domowe.TypyDanychOperatoryPakietyInstrukcje.zad9;

import java.util.Scanner;

public class KalkulatorStopni
{
    public static void main(String[] args)
    {
        Scanner scanner= new Scanner(System.in);
        double tempCelcius, tempFahrenheit;
        System.out.println("Podaj wartość w stopniach C");
        tempCelcius = scanner.nextInt();

        tempFahrenheit = 32 + (9.0 / 5.0) * tempCelcius;
        System.out.println("Stopnie F "+tempFahrenheit);
    }
}
