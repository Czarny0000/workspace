package pl.sda.zad15Metody;

import java.util.Scanner;

public class Metody2Kalkulator {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rodzaj działania");
        String rodzajDzialania = scanner.nextLine();
        System.out.println("Podaj pierwszą liczbe");
        int pierwszaLiczba = scanner.nextInt();
        System.out.println("Podaj drudą liczbe");
        int drugaLiczba = scanner.nextInt();
        kalkulator(rodzajDzialania, pierwszaLiczba,drugaLiczba);

    }

    public static void kalkulator(String rodzajDzialania,int pierwszaLiczba,int drugaLiczba) {
        switch (rodzajDzialania) {
            case "+":
                dodawanie(pierwszaLiczba,drugaLiczba);
                int wynik=dodawanie(pierwszaLiczba,drugaLiczba);
                System.out.println(wynik);
                break;
            case "-":
                odejmowanie(pierwszaLiczba,drugaLiczba);
                int wynik1 = odejmowanie(pierwszaLiczba,drugaLiczba);
                System.out.println(wynik1);
                break;
            case "*":
                mnozenie(pierwszaLiczba,drugaLiczba);
                int wynik2=mnozenie(pierwszaLiczba,drugaLiczba);
                System.out.println(wynik2);
                break;
            case"/":
                dzielenie(pierwszaLiczba,drugaLiczba);
                int wynik3 = dzielenie(pierwszaLiczba,drugaLiczba);
                System.out.println(wynik3);
                break;
            default:
                System.out.println("Niepoprawne działanie");
        }
    }

    public static int dodawanie(int a,int b) {
        return a+b;

    }
    static int odejmowanie(int a,int b){
        return a-b;
    }
    static int mnozenie(int a, int b){
        return a*b;
    }
    static int dzielenie(int a, int b){
        return a/b;
    }


}
