package pl.sda.zad8;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

/**
 *  program, który przyjmuje od użytkownika dzielnik oraz liczbę,
 *  a następnie drukuje na ekranie wszystkie liczby mniejsze od zadanej liczby podzielne przez zadany dzielnik
 */

public class ForLiczbaDzielnik
{
    public static void main(String[] args)
    {
        double dzielnik, liczba;
        Scanner scanner= new Scanner(System.in);
        System.out.println("Podaj dzielnik.");
        dzielnik = scanner.nextInt();
        System.out.println("Podaj liczbe.");
        liczba =scanner.nextInt();


        for(double i=1; i<liczba; i++)

        {
            if (i % dzielnik == 0)
                System.out.println(i);
        }

    }
}
