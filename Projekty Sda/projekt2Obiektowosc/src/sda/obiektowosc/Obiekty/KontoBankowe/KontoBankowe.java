package sda.obiektowosc.Obiekty.KontoBankowe;

public class KontoBankowe {

    private long numerKonta;
    private int stanKonta;


    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public long getNumerKonta() {
        return numerKonta;
    }
    public void setStanKonta(int stanKonta) {
        this.stanKonta = stanKonta;
    }

    public void wyswietlStanKonta(){
        System.out.println("Stan konta "+ numerKonta+" wynosi "+stanKonta);
    }
    public void wplacSrodki(int kwota){
        stanKonta+=kwota;
    }
    public int pobierzSrodki(int kwota){
        if(kwota>stanKonta){
            return 0;

        }
        else{
            stanKonta-=kwota;
            return kwota;
        }
    }


}
