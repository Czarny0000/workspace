package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5cWypelnianieTab;

public class Main {
    public static void main(String[] args) {
        int rozmiar = 10;
        double[][] tablica = new double[rozmiar][rozmiar];
        Zad5c matrix1 = new Zad5c();
        matrix1.czytaj_dane(tablica, rozmiar);
        matrix1.przetworz_dane(tablica, rozmiar);
        matrix1.wyswietl_wynik(tablica, rozmiar);
    }
}