package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5cWypelnianieTab;

import java.util.Random;

/**
 * zgodnie z zasadami programowania obiektowego program,
 * który w tablicy 10×10 umieszcza losowo na przekątnej
 * liczby od 0 do 9, a poza przekątną zera. Dodatkowo program
 * oblicza sumę liczb znajdujących się na przekątnej. Klasa powinna
 * zawierać trzy metody:
 */

public class Zad5c {

    public void czytaj_dane(double[][] macierz, int rozmiar) {
        int i, j;
        Random rand = new Random(); // generowanie liczby pseudolosowej
        for (i = 0; i < rozmiar; i++) {
            for (j = 0; j < rozmiar; j++) {
                if (i == j)
                    macierz[i][j] = Math.round(9 * (rand.nextDouble()));
// wpisywanie liczb pseudolosowych od 0 do 9 na przekątnej tablicy
                else
                    macierz[i][j] = 0;
// wpisywanie liczby 0 poza przekątną
            }
        }
    }

    public void przetworz_dane(double[][] macierz, int rozmiar) {
        int i;
        double suma = 0;
        for (i = 0; i < rozmiar; i++)
            suma = suma + macierz[i][i];
        System.out.println("Suma elementów na przekątnej wynosi " + (int) suma + ".");
// rzutowanie
    }

    public void wyswietl_wynik(double[][] macierz, int rozmiar) {
        int i, j;
        for (i = 0; i < rozmiar; i++) {
            for (j = 0; j < rozmiar; j++) {
                System.out.print((int) macierz[i][j] + " "); // rzutowanie
            }
            System.out.println();
        }
    }
}



