package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5dSortowanie;

public class Main {
    public static void main(String[] args) {
        int n = 6;
        int[] liczby = new int[n];
        Zad5d babelki = new Zad5d();
        babelki.czytaj_dane(liczby, n);
        babelki.przetworz_dane(liczby, n);
        babelki.wyswietl_wynik(liczby, n);
    }
}