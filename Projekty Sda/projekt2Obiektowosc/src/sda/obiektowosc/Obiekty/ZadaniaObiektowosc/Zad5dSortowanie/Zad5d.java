package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5dSortowanie;

/**
 * Zgodnie z zasadami programowania obiektowego program,
 * który sortuje n liczb (w programie jest ich sześć). Klasa
 * powinna zawierać trzy metody z parametrami:
 */

public class Zad5d {
    public void czytaj_dane(int [] liczby, int n)
    {
        int i;
        liczby[0] = 57;
        liczby[1] = 303;
        liczby[2] = 34;
        liczby[3] = 1025;
        liczby[4] = 8;
        liczby[5] = 20;
        System.out.print("Liczby nieposortowane to: ");
        for (i = 0; i < n; i++)
        {
            if (i < n - 1)
                System.out.print(liczby[i] + ", ");
            else
                System.out.print(liczby[i] + ".");
        }
        System.out.println();
    }
    public void przetworz_dane(int [] liczby, int n)
    {
        int i, j, x;
        for (i = 1; i <= n-1; i++)
        {
            for (j = n-1; j >= i; --j)
            {
                if (liczby[j-1] > liczby[j])
                {
                    x = liczby[j-1];
                    liczby[j-1] = liczby[j];
                    liczby[j] = x;
                }
            } // koniec pętli j
        } // koniec pętli i
    }
    public void wyswietl_wynik(int [] liczby, int n)
    {
        int i;
        System.out.print("Liczby posortowane to: ");
        for (i = 0; i < n; i++)
        {
            if (i < n - 1)
                System.out.print(liczby[i] + ", ");
            else
                System.out.print(liczby[i] + ".");
        }
        System.out.println();
    }
}


