package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5eRekurencjaSilnia;

/**
 * program, który rekurencyjnie oblicza kolejne wartości
 * n! dla n = 10 i wynik wyświetla na ekranie komputera.
 */

public class RekurencjaSilnia {
    public long silnia(int liczba)
    {
        long zwrot = 1;
        if (liczba >= 2)
        {
            zwrot = liczba*silnia(liczba-1);
        }
        return zwrot;
    }
}

