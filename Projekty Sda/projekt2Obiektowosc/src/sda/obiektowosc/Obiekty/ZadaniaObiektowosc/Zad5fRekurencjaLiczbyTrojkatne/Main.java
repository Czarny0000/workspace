package sda.obiektowosc.Obiekty.ZadaniaObiektowosc.Zad5fRekurencjaLiczbyTrojkatne;

/**
 * program, który rekurencyjnie znajduje 10 pierwszych
 * liczb trójkątnych i wyświetla je na ekranie komputera.
 * Wskazówka
 * W matematyce liczba trójkątna to taka, którą można zapisać w postaci sumy
 * kolejnych początkowych liczb naturalnych: Tn = 1 + 2 + 3 + (n – 1)
 * + n. Liczby należące do tego ciągu nazywane są trójkątnymi, gdyż można je
 * przedstawić w formie trójkąta. Na przykład #6 = 21.
 */

public class Main {

    public static void main(String[] args)
    {
        int i, n = 10;
        RekurencjaLiczbyTrojkatne liczby = new RekurencjaLiczbyTrojkatne();
        System.out.println("Program znajduje 10 pierwszych  liczb trójkątnych.");
        for (i = 1; i <= n; i++)
        {
            System.out.println("#" + i + " = " + liczby.triangle(i));
        }
    }
}
