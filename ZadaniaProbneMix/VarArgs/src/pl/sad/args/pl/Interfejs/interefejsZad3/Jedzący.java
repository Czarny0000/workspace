package pl.sad.args.pl.Interfejs.interefejsZad3;

public interface Jedzący {
    void jedz(Pokarm pokarm);
    int ilePosilkowZjedzone();
    int GramówZjedzone();
}
