package pl.sad.args.pl.Interfejs.interefejsZad3;

 public class Pokarm {
 private   String nazwa;
 private   TypPokarmu typPokarmu;
 private   double waga;

     public String getNazwa() {
         return nazwa;
     }

     public TypPokarmu getTypPokarmu() {
         return typPokarmu;
     }

     public double getWaga() {
         return waga;
     }

     public Pokarm(String nazwa, TypPokarmu typPokarmu, double waga) {
         this.nazwa = nazwa;
         this.typPokarmu = typPokarmu;
         this.waga = waga;

     }
 }
