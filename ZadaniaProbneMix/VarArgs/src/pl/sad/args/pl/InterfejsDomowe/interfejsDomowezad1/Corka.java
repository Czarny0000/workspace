package pl.sad.args.pl.InterfejsDomowe.interfejsDomowezad1;

public class Corka implements CzłonekRodziny {
    String imie;
    String ranga;

    public Corka(String imie, String ranga) {
        this.imie = imie;
        this.ranga = ranga;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Jestem matka");

    }

    @Override
    public boolean jestDorosly() {
        System.out.println();
        return true;
    }
}
