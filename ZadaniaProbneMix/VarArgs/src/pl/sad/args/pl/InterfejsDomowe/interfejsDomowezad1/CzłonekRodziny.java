package pl.sad.args.pl.InterfejsDomowe.interfejsDomowezad1;

public interface CzłonekRodziny {

    void przedstawSie();
    boolean jestDorosly();
}
