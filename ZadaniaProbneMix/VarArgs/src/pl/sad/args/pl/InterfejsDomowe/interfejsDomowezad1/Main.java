package pl.sad.args.pl.InterfejsDomowe.interfejsDomowezad1;

public class Main {
    public static void main(String[] args) {

        CzłonekRodziny dawid=new Syn("Dawid","syn","nie jest dorosly");
        CzłonekRodziny krzys=new Ojciec("krzys","ojciec");
        CzłonekRodziny jola=new Corka ("jola","corka");
        CzłonekRodziny kasia=new Matka("kasia","matka");

        CzłonekRodziny[] rodzina=new CzłonekRodziny[]{dawid,krzys,jola,kasia};
        for (CzłonekRodziny czlonek:rodzina) {
            czlonek.przedstawSie();
            System.out.println(czlonek.jestDorosly());

        }

        dawid.przedstawSie();
        dawid.jestDorosly();

        krzys.przedstawSie();

        krzys.jestDorosly();
        System.out.println(krzys.jestDorosly());
    }
}
