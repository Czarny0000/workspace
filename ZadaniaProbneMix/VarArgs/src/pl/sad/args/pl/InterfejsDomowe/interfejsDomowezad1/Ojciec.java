package pl.sad.args.pl.InterfejsDomowe.interfejsDomowezad1;

public class Ojciec implements CzłonekRodziny {
    String imie;
    String ranga;

    public Ojciec(String imie, String ranga) {
        this.imie = imie;
        this.ranga = ranga;
    }

    @Override
    public void przedstawSie() {
        System.out.println("jestem Ojcem");

    }

    @Override
    public boolean jestDorosly() {

        return true;
    }
}
