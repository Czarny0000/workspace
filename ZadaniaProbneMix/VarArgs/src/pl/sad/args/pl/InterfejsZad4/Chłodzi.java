package pl.sad.args.pl.InterfejsZad4;

public interface Chłodzi {
    double pobierzTemp();
    void schlodz();

    default void wyswietlTemp() {
        System.out.println("aktulna temperatura " + pobierzTemp());
    }
}
