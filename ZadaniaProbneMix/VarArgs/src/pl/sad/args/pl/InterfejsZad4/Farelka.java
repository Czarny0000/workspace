package pl.sad.args.pl.InterfejsZad4;

public class Farelka implements Grzeje {
private int temperatura=15;

    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void zwiekszTemp() {
        temperatura++;
        System.out.println("Tempertaura zwiekszyła sie do"+ temperatura);

    }
}
