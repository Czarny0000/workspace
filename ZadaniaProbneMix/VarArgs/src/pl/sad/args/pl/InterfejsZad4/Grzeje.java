package pl.sad.args.pl.InterfejsZad4;

public interface Grzeje {
    double pobierzTemp();
    void zwiekszTemp();

    default void wyswietlTemp() {
        System.out.println("aktulna temperatura "+ pobierzTemp());
    }

}
