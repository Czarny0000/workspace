package pl.sad.args.pl.InterfejsZad4;

public class Wiatrak implements Chłodzi {
    private int temperatura=15;

    @Override
    public double pobierzTemp() {
        return temperatura;
    }

    @Override
    public void schlodz() {

        temperatura--;
        System.out.println("temperatura zmieszyła sie o "+ temperatura);
    }
}
