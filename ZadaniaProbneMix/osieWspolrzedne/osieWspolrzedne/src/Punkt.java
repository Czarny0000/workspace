public class Punkt {
    int x;
    int y;

    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Punkt{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}




















//    Utwórz klasę Punkt o polach x:int i y:int – obie wartości zainicjuj w konstruktorze klasy.
//        Nadpisz metode .toString() aby zwracała informacje o współrzędnych punktu np. „(5,12)” dla x=5 i y=12.
