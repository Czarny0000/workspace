package ZadanieException;

import java.util.Scanner;

public class zad2 {
    public class RownanieKwadratowe {
        double a;
        double b;
        double c;
        double delta;
        char liczbaPierwiastkow;



        public RownanieKwadratowe(double a, double b, double c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public double obliczDelte(){

            delta=b*b-4*a*c;

            if(delta < 0){
                throw new DeltaLessZero("Delta mniejsza niz");
            }
            return delta;
        }
        public double obliczX1dlaDeltyRownejZero(){
            double wynik;
            wynik= -b / (2 * a);
            return wynik;

        }
        public double obliczX1(){
            double wynik;
            wynik=(-b-Math.sqrt(delta))/(2*a);

            return wynik;
        }
        public double obliczX2(){
            double wynik;
            wynik=(-b+Math.sqrt(delta))/(2*a);

            return wynik;
        }

        public void trojmian (){
            obliczDelte();

            if (delta < 0) liczbaPierwiastkow = 0;
            if (delta == 0) liczbaPierwiastkow = 1;
            if (delta > 0) liczbaPierwiastkow = 2;

            switch (liczbaPierwiastkow) {

                case 0:
                    System.out.print("brak pierwiastków rzeczywistych.");
                    break;
                case 1:
                    obliczX1dlaDeltyRownejZero();
                    System.out.printf("trójmian ma jeden pierwiastek podwójny\n" +
                            "x1 = " + "%2.2f.\n", obliczX1dlaDeltyRownejZero());
                    break;
                case 2: {
                    obliczX1();
                    obliczX2();
                    System.out.println("trójmian ma dwa pierwiastki");
                    System.out.printf("x1 = " + "%2.2f,\n", obliczX1());
                    System.out.printf("x2 = " + "%2.2f.\n", obliczX2());
                }
                break;
            }
        }
}}
