import java.time.DayOfWeek;
import java.time.LocalDate;

public class CalendarTest
{
    public static void main(String[]args)
    {
        LocalDate date = LocalDate.now();//Tworzymy obiekt kalendarza, który inicjujemy aktualna datą
        int month = date.getMonthValue();//pobieramy aktualny miesiąc
        int today = date.getDayOfMonth();// pobieramy aktualny dzień

        date = date.minusDays(today - 2);//ustawienie na poczatek miesiaca
        DayOfWeek weekday = date.getDayOfWeek();
        int value = weekday.getValue();// 1=poniedziałek, 7=niedziela

        System.out.println(" Pn  Wt  Śr  Czw  P  So  Nd");
        for (int i = 1;i < value;i++)
            System.out.print("  ");
        while (date.getMonthValue() == month)
        {
            System.out.printf("%3d",date.getDayOfMonth());
            if(date.getDayOfMonth() == today)
                System.out.print("*");
            else
                System.out.print(" ");
            date = date.plusDays(1);
            if(date.getDayOfWeek().getValue() == 1) System.out.println();
        }
        if(date.getDayOfWeek().getValue() != 1) System.out.println();


    }
}
