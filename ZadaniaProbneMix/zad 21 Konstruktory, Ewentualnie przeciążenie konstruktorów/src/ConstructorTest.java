import java.util.Random;

public class ConstructorTest
{
    public static void main(String[]args)
    {
        //Wstawianie do tablicy staff 3 obiektow klasy Employee
        Employee[] staff = new Employee[3];

        staff[0] = new Employee("Hubert",40000);
        staff[1] = new Employee(60000);
        staff[2] = new Employee();

        for(Employee e : staff)
            System.out.println("Name= " + e.getName() + " id= " + e.getId() + " salary + " + e.getSalary());


    }
}
class Employee
{
    private static int nextId;

    private int id;
    private String name = "";  //inicjalizacja zmiennej składowej obiektu
    private double salary;


    static// Statyczny blok inicjujacy
    {
        Random generator = new Random();
        nextId = generator.nextInt(10000);//Ustawienie zmiennej nextId na losową liczbę 0-9999
    }
    //BLOK INICIUJĄCY OBIEKTÓW
    {
        id = nextId;
        nextId++;
    }

    // Trzy konstruktory przeciążone
    public Employee(String n, double s)
    {
        name = n;
        salary = s;
    }

    public Employee(double s)
    {
        this("Employee #" + nextId, s);// Wywołanie konstruktora Employee(String, double).
    }

    // Konstruktor domyslny
    public Employee()
    {
        //Zmienna name zainicjalizowana wartością ""
        //Zmienna salary nie jest jawnie ustawiona -- inicjalizacja wartością 0
        //Zmienna id jest inicjalizowana w bloku inicjującym
    }

    public String getName()
    {
        return name;
    }

    public double getSalary()
    {
        return salary;
    }

    public int getId() {
        return id;
    }
}
