import java.awt.event.ActionListener;
import java.awt.event.*;
import java.awt.*;
import java.util.Date;
import java.util.Timer;
import javax.swing.*;

public class TimetTest
{
    public static void main(String[] agrs)
    {
        ActionListener listener = new TimePrinter();

        //Kontsrukcja zegara wywołującego obiekt nasłuchujacy co 10 sekund.

        javax.swing.Timer t = new javax.swing.Timer(2000, listener);
        t.start();

        JOptionPane.showMessageDialog(null, "Zamknąć program?");
        System.exit(0);
    }

 static class TimePrinter implements ActionListener {
     @Override
     public void actionPerformed(ActionEvent e)
     {
         System.out.println("Kiedy usłyszysz dźwiek będzie godzina" + new Date());
         Toolkit.getDefaultToolkit().beep();

     }
 }
}
