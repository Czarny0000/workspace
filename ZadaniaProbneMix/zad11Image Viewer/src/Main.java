import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Main {

    /**
     * Program do przeglądania obrazów.
     * @version 1.22 2007-05-21
     * @author Cay Horstmann
     */
    public static class ImageViewer
    {
        public static void main(String[] args)
        {
            EventQueue.invokeLater(new Runnable()
            {
                public void run()//metoda w obiekcie runnable
                {
                    JFrame frame = new ImageViewerFrame();//tworzy program do przegladania zdjęć
                    frame.setTitle("ImageViewer");// nadaje tutuł ramce
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// zamyka program po nacisnieciu czerwonego krzyzyka
                    frame.setVisible(true);// pokazuje/wyświetla na ekranie ramke  //do tej pory program znikal ale działał
                }
            });
        }
    }
    /**
     * Ramka z etykietą wyświetlająca obraz.
     */
    static class ImageViewerFrame extends JFrame
    {private JLabel label;
        private JFileChooser chooser;
        private static final int DEFAULT_WIDTH = 300;
        private static final int DEFAULT_HEIGHT = 400;
        public ImageViewerFrame()
        {
            setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
// Użycie etykiety do wyświetlenia obrazów.
            label = new JLabel();
            add(label);
// Dodawanie opcji wyboru obrazu.
            chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
// Pasek menu.
            JMenuBar menuBar = new JMenuBar();//Pasek Menu
            setJMenuBar(menuBar);
            JMenu menu = new JMenu("Plik");// menu o nazwie "Plik"
            menuBar.add(menu);// Do paska menu dodajemy menu o nazwie plik
            JMenuItem openItem = new JMenuItem("Otwórz");// Tworzymy JMenuItem
            menu.add(openItem);//Do JMenu o nazwie "Plik" dodajemy JMenuItem o nazwie "Otworz"
            openItem.addActionListener(new ActionListener()// dodajemy akcje do openItem
            {
                public void actionPerformed(ActionEvent event)
                {
// Wyświetlenie okna dialogowego wyboru pliku.
                    int result = chooser.showOpenDialog(null);
// Jeśli plik został wybrany, ustawiamy go jako ikonę etykiety.
                    if (result == JFileChooser.APPROVE_OPTION)
                    {
                        String name = chooser.getSelectedFile().getPath();
                        label.setIcon(new ImageIcon(name));
                    }
                }
            });
            JMenuItem exitItem = new JMenuItem("Zakończ");
            menu.add(exitItem);
            exitItem.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent event)
                {
                    System.exit(0);
                }
            });
        }
    }

}
