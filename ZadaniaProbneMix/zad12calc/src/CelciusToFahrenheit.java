import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CelciusToFahrenheit extends JFrame implements ActionListener
{
    private JLabel lCelcius, lFahrenheit;
    private JTextField tCelcius, tFahrenheit;
    private JButton bKonwertuj;
    private ButtonGroup bgRozmiar;
    private JRadioButton rbMały, rbSredni, rbDuzy;
    private JCheckBox chWielkie;
    private double tempCelcius, tempFahrenheit;

    public CelciusToFahrenheit()
    {

        setSize(400, 300);
        setTitle("Przeliczanie stopni");
        setLayout(null);

        lCelcius = new JLabel("stopnie C:");
        lCelcius.setBounds(20, 20, 150, 20);
        add(lCelcius);

        tCelcius = new JTextField("");
        tCelcius.setBounds(170, 20, 150, 20);
        add(tCelcius);
        tCelcius.addActionListener(this);
        tCelcius.setToolTipText("Podaj temp. w st. C");

        lFahrenheit = new JLabel("stopnie F:");
        lFahrenheit.setBounds(20,70,150,20);
        add(lFahrenheit);

        tFahrenheit = new JTextField("");
        tFahrenheit.setBounds(170,70,150,20);
        add(tFahrenheit);

        bKonwertuj = new JButton("Konwertuj");
        bKonwertuj.setBounds(20,100,150,20);
        add(bKonwertuj);
        bKonwertuj.addActionListener(this);

        chWielkie = new JCheckBox("Wielkie Litery");
        chWielkie.setBounds(150,120,150,20);
        add(chWielkie);
        chWielkie.addActionListener(this);

        bgRozmiar = new ButtonGroup();
        rbMały = new JRadioButton("Mały", true);
        rbMały.setBounds(50,150,100,20);
        bgRozmiar.add(rbMały);
        add(rbMały);
        rbMały.addActionListener(this);

        rbSredni = new JRadioButton("Sredni", false);
        rbSredni.setBounds(150,150,100,20);
        bgRozmiar.add(rbSredni);
        add(rbSredni);
        rbSredni.addActionListener(this);

        rbDuzy = new JRadioButton("Duzy",false);
        rbDuzy.setBounds(250,150,100,20);
        bgRozmiar.add(rbDuzy);
        add(rbDuzy);
        rbDuzy.addActionListener(this);


    }

    public static void main (String[]args)
    {
        CelciusToFahrenheit aplikacja = new CelciusToFahrenheit();
        aplikacja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        aplikacja.setVisible(true);
    }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            Object źródło = e.getSource();//pobieranie zrodla
            if(źródło == bKonwertuj|| źródło == tCelcius)
            {
                tempCelcius = Double.parseDouble(tCelcius.getText());
                tempFahrenheit = 32 + (9.0 / 5.0) * tempCelcius;
                tFahrenheit.setText(String.valueOf(tempFahrenheit));
            }
            else if(źródło == chWielkie)
            {
                if (chWielkie.isSelected()==true)//Jeśli wybrano wielkie
                {
                    tFahrenheit.setFont(new Font("SansSerif", Font.BOLD, 22));// to czcionka  stopni F =18 pogrobiona
                }
                else//Jeśli nie wybrano wielkie
                {
                    tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 12));// to czcionka  stopni F =12 zwykla
                }
            }
            else if(źródło == rbMały)
            {
                tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 12));
            }
            else if(źródło == rbSredni)
            {
                tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 16));
            }
            else if(źródło == rbDuzy)
            {
                tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 20));
            }



        }

}

