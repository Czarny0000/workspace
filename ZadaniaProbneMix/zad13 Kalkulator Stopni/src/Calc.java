import javax.swing.*;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;// potrzebne do wywołania zdarzenia kiedy przesuniemy wskaznik na sliderze
import javax.swing.event.ChangeListener;// potrzebne do odczytania wartosci ze sliderow
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;// akcja po nasisnieciu buttona,zaznaczeniu checkbox itp

public class Calc extends JFrame implements ActionListener, ChangeListener
{
    private JLabel lCelcius, lFahrenheit;// Deklaruję, że będą 2 etykiety w JLabel
    private JSlider sCelcius, sFahrenheit;//Deklaruję, że będą 2 Slidery w JSlider
    private int ttCelcius, ttFahrenheit;// Deklaruję zmienne
    private JTextField tCelcius, tFahrenheit;//Deklaruję ramki do wpisywania
    private JButton bKonwertuj;//Deklaruję przycisk/button
    private ButtonGroup bgRozmiar;//Deklaruję grupę przycisków
    private JRadioButton rbMały, rbSredni, rbDuzy;//Deklaruję małe buttony do wyboru,ktory ma być zaznaczony
    private JCheckBox chWielkie;//Deklaruję ramka do zaznaczenia, bądź odznaczenia o nazwie "chWielkie"
    private double tempCelcius, tempFahrenheit;//Deklaruję zmienne z wartością po przecinku

    public Calc()
    {

        setSize(400, 300);//wymiary ramki
        setTitle("Przeliczanie stopni");//tytuł
        setLayout(null);// ustawia managera Layout na null

        sCelcius = new JSlider(0,100,0);// nowy slider o dlugosci 100
        sCelcius.setBounds(50,35,300,36);//Współrzędne wyswietlania Slidera
        sCelcius.setMajorTickSpacing(20);// Ustawia DUŻĄ przedziałkę co 20 jednostek
        sCelcius.setMinorTickSpacing(5);// Ustawia MAŁĄ przedziałkę co 5 jednostek
        sCelcius.setPaintLabels(true);//robi etykiety co 20 widoczne
        sCelcius.setPaintTicks(true);// przedziałka co 5 staje sie widoczna
        add(sCelcius);// Dodaje Slider do okna
        sCelcius.addChangeListener(this);// Dodaje Actionlisnera do Slidera

        sFahrenheit = new JSlider(30,212,30);   //JAK WYŻEJ
        sFahrenheit.setBounds(50,190,300,36);//JAK WYŻEJ
        sFahrenheit.setMajorTickSpacing(20);//JAK WYŻEJ
        sFahrenheit.setMinorTickSpacing(5);//JAK WYŻEJ
        sFahrenheit.setPaintLabels(true);//JAK WYŻEJ
        sFahrenheit.setPaintTicks(true);//JAK WYŻEJ
        add(sFahrenheit);//JAK WYŻEJ
        sFahrenheit.addChangeListener(this);//JAK WYŻEJ

        lCelcius = new JLabel("stopnie C:");// Tworzy etykietę nazywającą sie lCelciu i nadaje jej tyuł, który będzie się wuswietlał
        lCelcius.setBounds(20, 5, 150, 18);// Wielkość i współrzędne etykiety
        add(lCelcius);// dodaje etykiete do okna

        tCelcius = new JTextField("");// Tworzy ramkę do wpisywania tekstu, bądź wartości
        tCelcius.setBounds(170, 5, 150, 18);// Wielkość i współrzędne
        add(tCelcius);//dodaje do okna
        tCelcius.addActionListener(this);//Dodaje akcje/SLUCHA CZY ROZPOCZNIESZ AKCJE ZA POMOCA tCelcius(po wpisaniu i naciścieciu enter)
        tCelcius.setToolTipText("Podaj temp. w st. C");// po najechaniu myszką na pole wyświetla się podpowiedź.

        lFahrenheit = new JLabel("stopnie F:");//JAK WYŻEJ
        lFahrenheit.setBounds(20,75,150,18);//JAK WYŻEJ
        add(lFahrenheit);//JAK WYŻEJ

        tFahrenheit = new JTextField("");//JAK WYŻEJ
        tFahrenheit.setBounds(170,75,150,18);//JAK WYŻEJ
        add(tFahrenheit);//JAK WYŻEJ
        tFahrenheit.addActionListener(this);//JAK WYŻEJ
        tFahrenheit.setToolTipText("Podaj temp. w st. F");//JAK WYŻEJ

        bKonwertuj = new JButton("Konwertuj");//Tworzy nowy przycisk bKonwertuj o nazwie"Konwertuj"
        bKonwertuj.setBounds(20,100,150,20);// Wielkość i współrzędne przycisku
        add(bKonwertuj);//Dodaje przycisk do okna
        bKonwertuj.addActionListener(this);//Akcja do bKonwertuj

        chWielkie = new JCheckBox("Wielkie Litery");// Tworzy nowy checkbox
        chWielkie.setBounds(150,120,150,20);
        add(chWielkie);
        chWielkie.addActionListener(this);

        bgRozmiar = new ButtonGroup();// Nowa grupa przycisków bgRozmiar
        rbMały = new JRadioButton("Mały", false);// false znaczy ze nie jest zaznaczone po włączeniu bo
        // moze tak byc, bo jest alternatywa(niezaznaczony checkbox) inaczej powinno byc true zeby jeden byl zaznaczony na starcie!!!
        rbMały.setBounds(50,150,100,20);
        bgRozmiar.add(rbMały);// Dodajemy radiobuttony do grupy przyciskow
        add(rbMały);
        rbMały.addActionListener(this);

        rbSredni = new JRadioButton("Sredni", false);
        rbSredni.setBounds(150,150,100,20);
        bgRozmiar.add(rbSredni);
        add(rbSredni);
        rbSredni.addActionListener(this);

        rbDuzy = new JRadioButton("Duzy",false);
        rbDuzy.setBounds(250,150,100,20);
        bgRozmiar.add(rbDuzy);
        add(rbDuzy);
        rbDuzy.addActionListener(this);


    }

    public static void main (String[]args)
    {
        Calc aplikacja = new Calc();// Tworzy apke calc
        aplikacja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Wyłącza apkilacje po zamknieci okna
        aplikacja.setVisible(true);// pokazuje sie na ekranie
    }

    @Override
    public void actionPerformed(ActionEvent e)// dodawanie akcji do Actionlisner
    {
        Object źródło = e.getSource();//pobieranie zrodla, akcji źródło akcji nazywa sie "źródło" bo = e.getSource()
        if(źródło == bKonwertuj|| źródło == tCelcius||źródło==sCelcius)// jesli zrodlem akcji jest jedna z wymienionych...
            //jesli zrodło pochodzi z pola tekstowego, wszystko dziala po wpisaniu i naciśnięciu enter!
        {//...to spelnia sie warunek
            tempCelcius = Double.parseDouble(tCelcius.getText());// odczytuje wartość podaną w polu tekstowym stopni celciusa
            tempFahrenheit = 32 + (9.0 / 5.0) * tempCelcius;//oblicza stopnie F
            tFahrenheit.setText(String.valueOf(tempFahrenheit));//Wpisuje wynik czyli tempFahrenheit w polu textowym tFahrenheit
        }
        else if(źródło == tFahrenheit)
        {
            tempFahrenheit = Double.parseDouble(tFahrenheit.getText());
            tempCelcius = ((tempFahrenheit-32)*5/9);
            tCelcius.setText(String.valueOf(tempCelcius));
        }

        else if(źródło == chWielkie)// Jeśli zrodłem akcji jest checkbutton
        {
            if (chWielkie.isSelected()==true)//Jeśli zrodlem akcji jest ZAZNACZONY checkbutton
            {
                tFahrenheit.setFont(new Font("SansSerif", Font.BOLD, 18));// to czcionka  stopni C =18 pogrobiona
                tCelcius.setFont(new Font("SansSerif", Font.BOLD, 18));// to czcionka  stopni F =18 pogrobiona
            }
            else//Jeśli Checkbutton "Wielkie" nie jest zaznaczony
            {
                tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 12));//to czcionka  stopni C =12 zwykla
                tCelcius.setFont(new Font("SansSerif", Font.PLAIN, 12));// to czcionka  stopni F =12 zwykla
            }
        }
        // Analogicznie do tego co wyżej
        else if(źródło == rbMały)
        {
            tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 10));
            tCelcius.setFont(new Font("SansSerif", Font.PLAIN, 10));
        }
        else if(źródło == rbSredni)
        {
            tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 14));
            tCelcius.setFont(new Font("SansSerif", Font.PLAIN, 14));
        }
        else if(źródło == rbDuzy)
        {
            tFahrenheit.setFont(new Font("SansSerif", Font.PLAIN, 18));
            tCelcius.setFont(new Font("SansSerif", Font.PLAIN, 18));
        }



    }

    @Override
    public void stateChanged(ChangeEvent e)//Akcje do Slidera
    {

        ttCelcius = sCelcius.getValue();// pobiera wartość ze slidera do zmiennej ttCelcius
        tCelcius.setText(String.valueOf(ttCelcius));// wpisuje w ramke tCelcius wartość ttCelciuc(wczesniej pobrana ze slidera)
        tempFahrenheit = (int) Math.round(32 + (9.0 / 5.0) * ttCelcius);// Oblicza stopnie F( a zapisane jest w ten sposób, ponieważ
        // tempFahrenheit jest Double, ttCelcius jest Int!!!!
        tFahrenheit.setText(String.valueOf(tempFahrenheit));// po obliczeniu tempFahrenheit wpisuje wynik w ramke tFahrenheit
        sFahrenheit.setValue((int) tempFahrenheit);//NIe pamietam, ale chyba miało ustawiac slider na danej wartości, ale cos nie pykło
        // pewnie dlatego ze uzywalem zmiennych int i double jak mi pasowalo i cos pojebalem.

        ttFahrenheit = sFahrenheit.getValue();
        tFahrenheit.setText(String.valueOf(ttFahrenheit));
        tempCelcius = (int)Math.round((tempFahrenheit-32)*5/9);
        tCelcius.setText(String.valueOf(tempCelcius));
        sCelcius.setValue((int) tempCelcius);

    }
}

