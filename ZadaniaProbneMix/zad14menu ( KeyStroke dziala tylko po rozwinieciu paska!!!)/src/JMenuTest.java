import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class JMenuTest extends JFrame implements ActionListener//klasa poszerzona o metode JFrame i zaimplementowanu ActionListener
{

    JMenuBar menuBar;
    JButton bSzukaj, bWybierzKolor;
    JPopupMenu popup;
    JMenu menuPlik, menuNarzedzia,menuLookAndFeel, menuOpcje, menuPomoc;
    JMenuItem mOtwórz, mZapisz, mWyjście, mNarz1, mNarz2,mMetal, mNimbus,mWindows, mpKopiuj, mpWklej, mpDołącz, mOpcja1, mOprogramie;
    JCheckBoxMenuItem chOpcja2;
    JComboBox colorcombo;
    JTextField tSzukany;
    JTextArea Notatnik;// pole do pisania
    String wybranyTekst;
    JPasswordField pHasło;
    JLabel lhasło;

    public JMenuTest()
    {
        setTitle("demonstracja JMenubar");
        setSize(600,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);

        lhasło = new JLabel("Hasło");
        lhasło.setBounds(50,375,150,20);
        add(lhasło);

        pHasło = new JPasswordField();//Tworzymy pole do wpisywania hasła
        pHasło.setBounds(50,400,150,20);// współrzedne i wymiary pola do wpisywania hasla
        pHasło.addActionListener(this);//akcja
        add(pHasło);// dodajemy do okna
        pHasło.setToolTipText("Wpisz hasło");// podpowiedz, ktora wyswietla sie po najechaniu myszka

        menuBar = new JMenuBar();// Tworzymy pasek menu
        setJMenuBar(menuBar);// dodajemy menubar
        menuPlik = new JMenu("Plik");// Tworzymy menuPlik
        menuBar.add(menuPlik);//Dodajemu menuPlik do menuBar
//Tworzymy JMenuItemy, dodajemy, Nazwe, Akcje i dodajemy je wszystkie do menuPlik
        mOtwórz = new JMenuItem("Otwórz",'O');//mnemonic rozpoczyna akcje po naciśnieciu O. ZEBY GO DODAĆ WPISUJEMY
        mOtwórz.addActionListener(this);                 // PO PRZECINKU LITERĘ ZAMKNIETĄ W APOSTROFACH
        mZapisz = new JMenuItem("Zapisz",'Z');
        mZapisz.addActionListener(this);
        mWyjście = new JMenuItem("Wyjście",'W');

        menuPlik.add(mOtwórz);
        menuPlik.add(mZapisz);
        menuPlik.addSeparator();//Dodaje kreske miedzy mZapisz, a mWyjscie
        menuPlik.add(mWyjście);

        mWyjście.addActionListener(this);
        mWyjście.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));//Ta komenda dodaje skrót klawiszowy

        menuLookAndFeel = new JMenu("Look And Feel");// Tworzymy nowe JMenu

        mMetal = new JMenuItem("Metal");// Tworzymy JMenuIteny i dodajemt Actionlistenery
        mMetal.addActionListener(this);
        mNimbus = new JMenuItem("Nimbus");
        mNimbus.addActionListener(this);
        mWindows = new JMenuItem("Windows");
        mWindows.addActionListener(this);
        menuLookAndFeel.add(mMetal);    //Dodajemy Metal, Nimbus i Windows do menu LookAndFeel
        menuLookAndFeel.add(mNimbus);
        menuLookAndFeel.add(mWindows);

        menuNarzedzia = new JMenu("Narzedzia");

        mNarz1 = new JMenuItem("Narz1");
        mNarz1.setEnabled(false);// Ustawia Narz1 jako nieaktywne
        mNarz2 = new JMenuItem("Narz2");
        mNarz2.addActionListener(this);

            menuOpcje = new JMenu("opcje");
            mOpcja1 = new JMenuItem("Opcja1");
            chOpcja2 = new JCheckBoxMenuItem("Opcja2");

        menuNarzedzia.add(mNarz1);
        mNarz1.setEnabled(false);
        menuNarzedzia.add(mNarz2);

          menuOpcje = new JMenu("opcje");
          mOpcja1 = new JMenuItem("Opcja1");// w menuOpcje tworzymy JMenuItem Opcja1
          chOpcja2 = new JCheckBoxMenuItem("Opcja2");// w menuOpcje tworzymy JCheckBoxMenuItem "opcja2"
            chOpcja2.addActionListener(this);//Do opcji2 dodajemy AkctionListener(Po zaznaczenui bedzie aktywowal Narz1)
            menuOpcje.add(mOpcja1);//do menuOpcje dodajemy JMenuItem Opcja1
            menuOpcje.add(chOpcja2);//Do menuOpcje dodajemy JCheckBoxMenuItem
        menuNarzedzia.add(menuOpcje);// do menu "narzedzia" dodajemy kolejne menu "opcje"

          menuOpcje.add(mOpcja1);
          menuOpcje.add(chOpcja2);
        menuNarzedzia.add(menuOpcje);




        menuPomoc = new JMenu("Pomoc");

        mOprogramie = new JMenuItem("O Programie");
        mOprogramie.addActionListener(this);
        menuPomoc.add(mOprogramie);

        Notatnik = new JTextArea();// nowe pole notatnika
        JScrollPane scrolpane = new JScrollPane(Notatnik);// Dodaje rolki do przewijania do ploa notatnika
        scrolpane.setBounds(50,50,300,300);
        add(scrolpane);
//Tworzymy ploe tekstowe do wpisywania szukanych fraz, okreslamy wspolrzedne i wymiary, dodajemy do okna i dajemy tooltip
        tSzukany = new JTextField();
        tSzukany.setBounds(50,350,100,20);
        add(tSzukany);
        tSzukany.setToolTipText("Wpisz szukaną frazę");
//Przycisk Szukaj
        bSzukaj = new JButton("Szukaj");
        bSzukaj.setBounds(200,350,100,20);
        add(bSzukaj);
        bSzukaj.addActionListener(this);
//Przycisk kolor
        bWybierzKolor = new JButton("Kolor");
        bWybierzKolor.setBounds(340,350,100,20);
        add(bWybierzKolor);
        bWybierzKolor.addActionListener(this);

//Popup Menu czyli to co sie rozwija po nacisniecui PPM
        popup = new JPopupMenu();
        mpKopiuj = new JMenuItem("Kopiuj");
        mpKopiuj.addActionListener(this);
        mpWklej = new JMenuItem("Wklej");
        mpWklej.addActionListener(this);
        mpDołącz = new JMenuItem("Dołącz");
        mpDołącz.addActionListener(this);

        popup.add(mpKopiuj);
        popup.add(mpWklej);
        popup.add(mpDołącz);

        Notatnik.setComponentPopupMenu(popup);// Dodanie popup menu do Notatnika

//Rozwijane opcje z kolorami tekstu do wyboru tzw Colorcombo
        colorcombo = new JComboBox();
        colorcombo.setBounds(400,400,100,20);
        colorcombo.addItem("Czarny");
        colorcombo.addItem("Żółty");
        colorcombo.addItem("Zielony");
        colorcombo.addItem("Niebieski");
        add(colorcombo);
        colorcombo.addActionListener(this);
//Dodajemy do JMenuBar wszystkie menu co robilismy powyzej
        setJMenuBar(menuBar);
        menuBar.add(menuPlik);
        menuBar.add(menuNarzedzia);
        menuBar.add(menuLookAndFeel);
        menuBar.add(Box.createHorizontalGlue());// Przykleja menuPomoc po przeciwnej stronie
        menuBar.add(menuPomoc);
    }

    @Override
    public void actionPerformed(ActionEvent e)// to sie robi samo w programie
    {
        Object z = e.getSource();// z = pobieranie zrodla

        if(z==mOtwórz)
        {
            JFileChooser fc = new JFileChooser();
            if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                File Plik = fc.getSelectedFile();
                //JOptionPane.showMessageDialog(null,"Wtbrany Plik to"+ Plik.getAbsolutePath());
                try
                {
                    Scanner skaner = new Scanner(Plik);
                    while (skaner.hasNext())
                        Notatnik.append(skaner.nextLine()+"\n");
                }
                catch (FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
            }
        }
        else if (z==mMetal)
        {
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");// ustawia styl na metal (po wpisaniu tej komendy
            } catch (ClassNotFoundException e1) {                          // program sam rozbil to na try/catch
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }
            SwingUtilities.updateComponentTreeUI(this);
        }

        else if (z==mNimbus)
        {
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }
            SwingUtilities.updateComponentTreeUI(this);
        }

        else if (z==mWindows)
        {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }
            SwingUtilities.updateComponentTreeUI(this);
        }

// to narazie trzeba zapamietac a nie rozkminiac
        else if(z==mZapisz)
        {
            JFileChooser fc = new JFileChooser();
            if(fc.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                File Plik = fc.getSelectedFile();
                try
                {
                    PrintWriter pw = new PrintWriter(Plik);
                    Scanner skaner = new Scanner(Notatnik.getText());
                    while(skaner.hasNext())
                        pw.println(skaner.nextLine()+"\n");
                    pw.close();

                }
                catch (FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
                //JOptionPane.showMessageDialog(null,"Wtbrany Plik to"+ Plik);
            }
        }

        if(z==mWyjście)
        {
            int odp = JOptionPane.showConfirmDialog(null, "Czy na pewno Wyjść","Pytanie",JOptionPane.YES_NO_OPTION);
            if (odp==JOptionPane.YES_OPTION)// pokazuje okno dialogowe z tekstem i tytulem, do wyboru odpowiedzi yes/no
            dispose();//jesli yes to zamyka
            else if (odp==JOptionPane.NO_OPTION)//jesli odp NO to wyswietla okno messagedialog
                JOptionPane.showMessageDialog(null, "Wiedziałem..");
            else if(odp==JOptionPane.CLOSED_OPTION)//jesli nacisniemy krzyzyk to wyswietla WarningMessage
                JOptionPane.showMessageDialog(null, "Czyli jednak nie wychodzimy durniu?", "Tytuł", JOptionPane.WARNING_MESSAGE);
        }
        if(z==chOpcja2)
        {
            if(chOpcja2.isSelected())// jesli opcja2 jest zaznaczona (checkbox)
                mNarz1.setEnabled(true);// to aktywuje Narz1
            else if (!chOpcja2.isSelected())// Jesli NIE Opcja2 jest zaznaczona ( Jesli nie jest zaznaczona pisane z wykrzyknikiem)
                mNarz1.setEnabled(false);//to Narz1 pozostaje nieaktywne
        }
        if(z==mNarz2)
        {
            String sMetry = JOptionPane.showInputDialog("Podaj Długość w metrach");//wyswietla okno polem do wpisywania tekstu
            double metry = Double.parseDouble(sMetry);// zmienia tryp string na typ liczbowy
            double stopy = metry/0.3048;// przelicza metry na stopy
            String sStopy = String.format("%.2f",stopy);
            JOptionPane.showMessageDialog(null,metry +"Metrów="+sStopy + "Stóp");// wyswietla rezultat w oknie info
        }
        if(z==mOprogramie)
            JOptionPane.showMessageDialog(null, "Program demonstruje wykorzystanie JMenuBar i Jmenu\n wersja 1.0","Tytuł", JOptionPane.INFORMATION_MESSAGE);
            //wyswietla okno informacyjne
    else if(z==bSzukaj)
    {
        String tekst = Notatnik.getText();// lancuch "tekst" = pobieranie tekstu z notatnika
        String szukane = tSzukany.getText(); // lancuch "szukane" = pobieranie tekstu z pola tSzukany
        String wystąpienia = "";
        int i=0;
        int index;
        int startIndex=0;
        while((index = tekst.indexOf(szukane, startIndex))!=-1)// pętla okresla indeks w polu tekst znalezionych fraz z pola szukane
        {
            startIndex = index + szukane.length();
            i++;
            wystąpienia = wystąpienia + " " + index;
        }
        JOptionPane.showMessageDialog(null,szukane + " wystąpiło " +i+ " razy: " + wystąpienia);
    }
//Otwiera palete kolorow do wyboru
    else if (z==bWybierzKolor)
    {
        Color Kolor = JColorChooser.showDialog(null,"Wybór Koloru", Color.BLUE);
        Notatnik.setForeground(Kolor);
    }
// Akcje do PopupMenu
    else if (z==mpKopiuj)

        wybranyTekst = Notatnik.getSelectedText();// pobiera/kopiuje zaznaczony tekst

    else if(z==mpWklej)
        Notatnik.insert(wybranyTekst, Notatnik.getCaretPosition());// wkleja "wybranyTekst"

    else if (z==mpDołącz)
        Notatnik.append("\n" + wybranyTekst);//Dołącza skopiowany tekst na koncu
// Przypisywanie konkretnuch kolorow pod colorcombo
    else if (z==colorcombo)
    {
        String color = colorcombo.getSelectedItem().toString();
        if (color.equals("Zielony"))
            Notatnik.setForeground(Color.GREEN);
        else if( color.equals("Niebieski"))
            Notatnik.setForeground(Color.BLUE);
        else if(color.equals("Żółty"))
            Notatnik.setForeground(Color.YELLOW);
        else if(color.equals("Czarny"))
            Notatnik.setForeground(Color.BLACK);
    }
    else if (z==pHasło)
            JOptionPane.showMessageDialog(null, String.valueOf(pHasło.getPassword()));
//jesli z==phaslo to pobiera haslo po nacisnieciu enter i wyswietla messagdialog z pobranym hasłem

    }

    public static void main(String[]args)
    {
        JMenuTest appMenu = new JMenuTest();
        appMenu.setDefaultCloseOperation(EXIT_ON_CLOSE);
        appMenu.setVisible(true);
    }
}
